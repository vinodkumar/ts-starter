import { Range, List }  from 'immutable';

//is prime
export const isPrime = (n: number): boolean =>
 !Range(2, n).some(i => n % i === 0);

//prime numbers
export const primeNumbers = (num: number): List<number>  => {
    if (num > 2) {
    return(Range(2, num).filter(isPrime).toList());
    }else {
      throw new Error('There are no primenumbers less than 2');
      }};

//immutable counter class with increment, decrement, getValue methods.
export class Counter {
  private readonly count: number ;
  constructor(count = 0) {
  this.count = count;
  }
  getValue = (): number => this.count;
  increment = (): Counter => new Counter(this.count + 1);
  decrement = (): Counter => new Counter(this.count - 1);
  }

//factorial
export  const factorial = (n: number): number => {
  if (n === 0) {
    return 1;
  }else if (n < 1) {
    throw new Error('factorial is not possible for a negative number');
  }else {
    return (Range(1, n + 1).reduce((a, b) => a * b, 1));
    }
};

//ncr
export const ncr = (n: number, r: number): number => {
  if ((n < 0) || (n < r)) {
    throw new Error('n value should be greater than zero and r');
  }else if (n === r) {
    return 1;
    }else {
      return(factorial(n) / (factorial(n - r) * factorial (r) ));
      }
};

//pascal line
export const pascalLine = (n: number) => {
  if (n < 1) {
    throw new Error('no pascalLine to numbers less than 1');
    }else {
      return Range(0, n).map(r => ncr(n - 1, r));
  }
};

// pascal triangle
export const pascalTriangle = (n: number) => {
   if (n < 1) {
     throw new Error ('no pascalTriangle to numbers less than 1');
     }else {
       return Range(1, n + 1).map(pascalLine);
     }
};

