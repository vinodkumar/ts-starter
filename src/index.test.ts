import { isPrime, primeNumbers, pascalTriangle,
         pascalLine, ncr, factorial, Counter } from './index';
import { List } from 'immutable';

//primenumbers
describe('PrimeNumbers', () => {
test('isPrime(0)', () => {
  expect(isPrime(0)).toBe(false);
});

test('isPrime(3)', () => {
  expect(isPrime(3)).toBe(true);
});

test('isPrime(6)', () => {
  expect(isPrime(6)).toBe(false);
});

test('isPrime(-7)', () => {
  expect(isPrime(-7)).toBe(false);
});

test ('primeNumbers (3)', () => {
    expect(primeNumbers(3)).toEqual(List([2]));
});

test ('primeNumbers (10)', () => {
    expect(primeNumbers(10)).toEqual(List([2, 3, 5, 7]));
});

test ('primeNumbers (11)', () => {
    expect(primeNumbers(11)).toEqual(List([2, 3, 5, 7]));
});

});

//ncr
describe('ncr', () => {
test ('ncr of 0,0', () => {
    expect(ncr(0, 0)).toBe(1);
});

test ('ncr of 5,0', () => {
    expect(ncr(5, 0)).toBe(1);
});

test ('ncr of 5,1', () => {
    expect(ncr(5, 1)).toBe(5);
});

test ('ncr of 5,5', () => {
    expect(ncr(5, 5)).toBe(1);
});


test ('ncr of 5,2', () => {
    expect(ncr(5, 2)).toBe(10);
});
});

//factorial
describe ('Factorial', () => {

test ('factorial(0)', () => {
    expect(factorial (0)).toBe(1);
});

test ('factorial(1)', () => {
    expect(factorial (1)).toBe(1);
});

test ('factorial(4)', () => {
    expect(factorial (4)).toBe(24);
});

test ('factorial(10)', () => {
    expect(factorial (10)).toBe(3628800);
});

});

//pascal line
describe('PascalLine', () => {
    test ('pascalLine(1)', () => {
    expect(pascalLine(1).toList()).toEqual(List([1]));
});

test ('pascalLine(2)', () => {
    expect(pascalLine(2).toList()).toEqual(List([1, 1]));
});

test ('pascalLine(3)', () => {
    expect(pascalLine(3).toList()).toEqual(List([1, 2, 1]));
});

test ('pascalLine(6)', () => {
    expect(pascalLine(6).toList()).toEqual(List([1, 5, 10, 10, 5, 1]));
});

});

//pascal triangle
describe('PascalTriangle', () => {
 test ('pascalTriangle (1)', () => {
     const result = pascalTriangle(1).map(x => x.toList()).toList();
    expect(result).toEqual(List([List([1])]));
});
test ('pascalTriangle (3)', () => {
     const result = pascalTriangle(3).map(x => x.toList()).toList();
    expect(result).toEqual(List([List([1]), List([1, 1]), List([1, 2, 1])]));
});

test ('pascalTriangle (4)', () => {
     const result = pascalTriangle(4).map(x => x.toList()).toList();
    expect(result).toEqual(List([List([1]), List([1, 1]), List([1, 2, 1]), List([1, 3, 3, 1])]));
});
});

//immutable counter class
describe('ImmmutableCounterClass', () => {
const myctr = new Counter(10);

test ('count', () => {
    expect(myctr.getValue()).toBe(10);
});


test ('increment ', () => {
    expect(myctr.increment().getValue()).toBe(11);
});

test ('decrement ', () => {
    expect(myctr.decrement().getValue()).toBe(9);
});

});
